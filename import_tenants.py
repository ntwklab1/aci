from acitoolkit import Session
import os


def login_to_apic(apic_url, username, password):
    session = Session(apic_url, username, password)
    session.login()
    return session


def get_tenants(session):
    tenants = session.get('/api/class/fvTenant.json').json()['imdata']
    return tenants


def generate_tenant_config(tenants):
    terraform_config = ""
    for tenant in tenants:
        tenant_name = tenant['fvTenant']['attributes']['name']
        tenant_dn = tenant['fvTenant']['attributes']['dn']
        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_tenant.{tenant_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_tenant" "{tenant_name}_{tn}" {{
    name = "{tenant_name}"
}}
'''
        terraform_config += resource_block
    return terraform_config


def get_bridge_domains(session):
    bridge_domains = session.get('/api/class/fvBD.json').json()['imdata']
    return bridge_domains


def generate_bd_config(bridge_domains):
    bdterraform_config = ""
    for bd in bridge_domains:
        bd_attributes = bd['fvBD']['attributes']
        bd_name = bd_attributes['name']
        tenant_dn = bd_attributes['dn']
        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_bridge_domain.{bd_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_bridge_domain" "{bd_name}_{tn}" {{
    name      = "{bd_name}"
    tenant_dn = "{tenant_dn}"
}}
'''
        bdterraform_config += resource_block
    return bdterraform_config


def get_vrfs(session):
    vrfs = session.get('/api/class/fvCtx.json').json()['imdata']
    return vrfs


def generate_vrf_config(vrfs):
    vrf_terraform_config = ""
    for vrf in vrfs:
        vrf_attributes = vrf['fvCtx']['attributes']
        vrf_name = vrf_attributes['name']
        tenant_dn = vrf_attributes['dn']
        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_vrf.{vrf_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_vrf" "{vrf_name}_{tn}" {{
    name      = "{vrf_name}"
    tenant_dn = "{tenant_dn}"
}}
'''
        vrf_terraform_config += resource_block
    return vrf_terraform_config


def get_app_epgs(session):
    app_epgs = session.get('/api/class/fvAEPg.json').json()['imdata']
    return app_epgs


def generate_app_epg_config(app_epgs):
    app_epg_terraform_config = ""
    for app_epg in app_epgs:
        app_epg_attributes = app_epg['fvAEPg']['attributes']
        app_epg_name = app_epg_attributes['name']
        tenant_dn = app_epg_attributes['dn']
        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_application_epg.{app_epg_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_application_epg" "{app_epg_name}_{tn}" {{
    name      = "{app_epg_name}"
    application_profile_dn = "{tenant_dn}"
}}
'''
        app_epg_terraform_config += resource_block
    return app_epg_terraform_config


def get_l3_outs(session):
    l3_outs = session.get('/api/class/l3extOut.json').json()['imdata']
    return l3_outs


def generate_l3_out_config(l3_outs):
    l3_out_terraform_config = ""
    for l3_out in l3_outs:
        l3_out_attributes = l3_out['l3extOut']['attributes']
        l3_out_name = l3_out_attributes['name']
        tenant_dn = l3_out_attributes['dn']

        l3_out_dn = l3_out['l3extOut']['attributes']['dn']
        l3_domain_profile_name = l3_out_dn.split('/')[-1]
        print(f"L3 Out DN: {l3_out_dn}")

        # # Check if the L3 Out has children
        # if 'children' in l3_out['l3extOut']:
        #     children = l3_out['l3extOut']['children']

        #     # Iterate over the children to find the L3 Domain Profile
        #     for child in children:
        #         if 'l3extRsEctx' in child:
        #             l3_domain_profile_dn = child['l3extRsEctx']['attributes']['tnFvCtxName']
        #             print(f"L3 Domain Profile DN: {l3_domain_profile_dn}")

        #         # Extract the L3 Domain Profile name
        #         l3_domain_profile_name = l3_domain_profile_dn.split('/')[-1]
        #         print(f"L3 Domain Profile Name: {l3_domain_profile_name}")

        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_l3_outside.{l3_out_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_l3_outside" "{l3_out_name}_{tn}" {{
    name      = "{l3_out_name}"
    tenant_dn = "{tenant_dn}"
    relation_l3ext_rs_ectx = "aci_l3_domain_profile.{l3_domain_profile_name}.id"
}}
'''
        l3_out_terraform_config += resource_block
    return l3_out_terraform_config


def get_logical_node_profiles(session):
    logical_node_profiles = session.get('/api/class/l3extLNodeP.json').json()['imdata']
    return logical_node_profiles


def generate_logical_node_profile_config(logical_node_profiles):
    lnprofile_terraform_config = ""
    for lnprofile in logical_node_profiles:
        lnprofile_attributes = lnprofile['l3extLNodeP']['attributes']
        lnprofile_name = lnprofile_attributes['name']
        tenant_dn = lnprofile_attributes['dn']
        tn = tenant_dn.replace("/", "_")
        print(f'- gitlab-terraform import "aci_logical_node_profile.{lnprofile_name}_{tn}" "{tenant_dn}"')
        resource_block = f'''
resource "aci_logical_node_profile" "{lnprofile_name}_{tn}" {{
    name      = "{lnprofile_name}"
    l3_outside_dn = "{tenant_dn}"
}}
'''
        lnprofile_terraform_config += resource_block
    return lnprofile_terraform_config


def logout_from_apic(session):
    session.close()


# ACI fabric connection details
apic_url = "https://10.10.20.14"
username = "admin"
password = "C1sco12345"

# Create a session to the APIC
session = login_to_apic(apic_url, username, password)

# Get the list of tenants
tenants = get_tenants(session)

# Generate the Terraform configuration for tenants
terraform_config = generate_tenant_config(tenants)

# Get the list of bridge domains
bridge_domains = get_bridge_domains(session)

# Generate the Terraform configuration for bridge domains
bdterraform_config = generate_bd_config(bridge_domains)

# Get the list of VRFs
vrfs = get_vrfs(session)

# Generate the Terraform configuration for VRFs
vrf_terraform_config = generate_vrf_config(vrfs)

# Get the list of Application EPGs
app_epgs = get_app_epgs(session)

# Generate the Terraform configuration for Application EPGs
app_epg_terraform_config = generate_app_epg_config(app_epgs)

# Get the list of L3 Outs
l3_outs = get_l3_outs(session)

# Generate the Terraform configuration for L3 Outs
l3_out_terraform_config = generate_l3_out_config(l3_outs)

# Get the list of Logical Node Profiles
logical_node_profiles = get_logical_node_profiles(session)

# Generate the Terraform configuration for Logical Node Profiles
lnprofile_terraform_config = generate_logical_node_profile_config(logical_node_profiles)

# Logout from the APIC
logout_from_apic(session)

# Save the Terraform configuration for tenants to a file
dir_name = os.path.dirname(os.path.abspath(__file__))
file_name = "import2.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, "w") as file:
    file.write(terraform_config)

# Save the Terraform configuration for bridge domains to a file
file_name = "import3.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, 'w') as file:
    file.write(bdterraform_config)

# Save the Terraform configuration for VRFs to a file
file_name = "import4.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, 'w') as file:
    file.write(vrf_terraform_config)

# Save the Terraform configuration for Application EPGs to a file
file_name = "import5.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, 'w') as file:
    file.write(app_epg_terraform_config)

# Save the Terraform configuration for L3 Outs to a file
file_name = "import6.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, 'w') as file:
    file.write(l3_out_terraform_config)

# Save the Terraform configuration for Logical Node Profiles to a file
file_name = "import7.tf"
import_file = os.path.join(dir_name, file_name)
with open(import_file, 'w') as file:
    file.write(lnprofile_terraform_config)
