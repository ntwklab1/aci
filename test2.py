from acitoolkit import Session

# ACI fabric connection details
apic_url = "https://10.10.20.14"
username = "admin"
password = "C1sco12345"

# Create a session to the APIC
session = Session(apic_url, username, password)
session.login()

# Get the list of L3 Outs
l3_outs = session.get('/api/class/l3extOut.json').json()['imdata']

# Print the L3 Outs and L3 Domain Profiles
for l3_out in l3_outs:
    l3_out_dn = l3_out['l3extOut']['attributes']['dn']
    print(f"L3 Out DN: {l3_out_dn}")

    # Check if the L3 Out has children
    if 'children' in l3_out['l3extOut']:
        children = l3_out['l3extOut']['children']

        # Iterate over the children to find the L3 Domain Profile
        for child in children:
            if 'l3extRsEctx' in child:
                l3_domain_profile_dn = child['l3extRsEctx']['attributes']['tnFvCtxName']
                print(f"L3 Domain Profile DN: {l3_domain_profile_dn}")